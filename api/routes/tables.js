import express from "express";
import { createTable, deleteTable, getTable, getTables, updateTable } from "../controllers/table.js";

import { verifyAdmin } from "../utils/verifyToken.js";

const router = express.Router();
//CREATE
router.post("/:Restaurantid", verifyAdmin, createTable);

//UPDATE

router.put("/:id", verifyAdmin, updateTable);
//DELETE
router.delete("/:id/:Restaurantid", verifyAdmin, deleteTable);
//GET

router.get("/:id", getTable);
//GET ALL

router.get("/", getTables);

export default router;