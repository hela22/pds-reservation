import express from "express";
import { countByCity, countByType, createResto, deleteResto, getResto, getRestos, updateResto } from "../controllers/restaurant.js";
import Restaurant from "../models/Restaurant.js";
import { verifyAdmin } from "../utils/verifyToken.js";
const router = express.Router();
router.post("/",verifyAdmin, createResto);
  router.delete("/:id", verifyAdmin,deleteResto );
  router.put("/:id", verifyAdmin,updateResto );
  router.get("/find/:id", getResto );
  router.get("/", getRestos );
  router.get("/countByCity", countByCity);
  router.get("/countByType", countByType);






export default router;