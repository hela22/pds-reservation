import Restaurant from "../models/Restaurant.js"

export const createResto = async(req,res,next)=>{
    const newResto = new Restaurant(req.body)
    try{
     const saveResto = await newResto.save()
     res.status(200).json(saveResto)
    }catch(err){
        next(err)
    }
}
export const updateResto = async(req,res,next)=>{
    try{
        const updateResto = await Restaurant.findByIdAndUpdate(req.params.id, { $set: req.body},{ new: true})
        res.status(200).json(updateResto)
       }catch(err){
           next(err)
       }
}
export const deleteResto = async(req,res,next)=>{
    try{
        await Restaurant.findByIdAndDelete(req.params.id)
        res.status(200).json("restaurant has been deleted")
       }catch(err){
           next(err)
       }
}
export const getResto = async(req,res,next)=>{
    try{
        const Resto = await Restaurant.findById(req.params.id)
        res.status(200).json(Resto)
       }catch(err){
           next(err)
       }
}
export const getRestos = async(req,res,next)=>{
  const { min, max, ...others } = req.query;
    try{
        const Restos = await Restaurant.find({
          ...others,
          cheapestPrice: { $gt: min | 1, $lt: max || 999 },
        }).limit(req.query.limit)
        res.status(200).json(Restos)
       }catch(err){
           next(err)
       }
}
export const countByCity = async (req, res, next) => {
    const cities = req.query.cities.split(",");
    try {
      const list = await Promise.all(
        cities.map((city) => {
          return Restaurant.countDocuments({ city: city });
        })
      );
      res.status(200).json(list);
    } catch (err) {
      next(err);
    }
  };
  export const countByType = async (req, res, next) => {
    try {
      const FastFoodCount = await Restaurant.countDocuments({ type: "Fast Food" });
      const PizzariaCount = await Restaurant.countDocuments({ type: "Pizzaria" });
      const ChaabiCount = await Restaurant.countDocuments({ type: "Chaabi" });
      const RestaurantCount = await Restaurant.countDocuments({ type: "Restaurant" });
      
  
      res.status(200).json([
        
        { type: "Fast Food", count: FastFoodCount },
        { type: "Pizzaria", count: PizzariaCount },
        { type: "Chaabi", count: ChaabiCount },
        { type: "Restaurant", count: RestaurantCount },
      ]);
    } catch (err) {
      next(err);
    }
  };
