import "./mailList.css"

const MailList = () => {
  return (
    <div className="mail" style={{ backgroundImage: `url(${require("./../../assets/img/res3.jpg")})`,backgroundSize: "cover",
    backgroundRepeat: "no-repeat",width: "100vw",height: "40vh",color: "#841f22"}}>
      <h1 className="mailTitle">Save time, save money !</h1>
      <span className="mailDesc">Sign up and we'll send the best deals to you</span>
      <div className="mailInputContainer">
        <input type="text" placeholder="Your Email" />
        <button>Subscribe</button>
      </div>
    </div>
  )
}

export default MailList