import "./footer.css";

const Footer = () => {
  return (
    <div className="footer">
      <div className="fLists">
        <ul className="fList">
          <li className="fListItem"  style={{color: "#F09408"}}>About</li>
          <li className="fListItem"  style={{color: "#F09408"}}>Contact</li>
          <li className="fListItem"  style={{color: "#F09408"}}>Are you a resturant ?</li>

        </ul>
        <ul className="fList">
          <li className="fListItem"  style={{color: "#F09408"}}>Frequently asked questions </li>
          <li className="fListItem"  style={{color: "#F09408"}}>We are hiring </li>
          <li className="fListItem"  style={{color: "#F09408"}}>Restaurant near me </li>
        </ul>
        <ul className="fList">
          <li className="fListItem"  style={{color: "#F09408"}}>Terms of use </li>
          <li className="fListItem"  style={{color: "#F09408"}}>Privacy and Cookies Statement</li>
          <li className="fListItem"  style={{color: "#F09408"}}>Cookie consent </li>
         
        </ul>
        <ul className="fList">
          <li className="fListItem"  style={{color: "#F09408"}}>Curtomer Service</li>
          <li className="fListItem"  style={{color: "#F09408"}}>Partner Help</li>
          <li className="fListItem"  style={{color: "#F09408"}}>Careers</li>
        </ul>
      </div>
  
    </div>
  );
};

export default Footer;
