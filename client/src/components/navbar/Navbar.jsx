import "./navbar.css";
import { Link } from "react-router-dom";
import { useContext } from "react";
import { AuthContext } from "../../context/AuthContext";
import logo from "./../../assets/img/logo.jpg";

import { useNavigate } from "react-router-dom";

const Navbar = () => {
  const navigate = useNavigate();
  const { user } = useContext(AuthContext);
  const handleLogin = () => {
    navigate("/login");
  };

  return (
    <div className="navbar">
    
    <div><img src={logo} style={{
   
      height: "90px",
      width: "150px", left : "10px",top : "15px",position: "fixed"}}></img></div>
      <div className="navContainer" >

        {user ? user.username : (
          <div className="navItems" >
          {!user && <button className="navButton" style={{position : "fixed", right : "10px",backgroundColor:  "#F09408",color: "white",textalign: "center",width: "100px",borderRadius: "4px",height:"50px",top : "40px",fontSize: "15px",fontweight: "400",
          fontfamily: "sans-serif",fontstyle: "normal"}}>REGISTER</button>}

          {!user &&  <button className="navButton" style={{position : "fixed", right : "120px",backgroundColor: "#F09408",color: "white",textalign: "center",width: "100px",  borderRadius: "4px",height:"50px",top : "40px",fontSize: "15px",
          fontweight: "400",fontfamily: "sans-serif",fontstyle: "normal"}} onClick={handleLogin}>LOGIN</button>}
         
          </div>
        )}
      </div>
    </div>
  );
};

export default Navbar;
